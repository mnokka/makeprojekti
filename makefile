# mika.nokka1@gmail.com - demo version 15.10.2016
# 
 
# the compiler: gcc for C program, define as g++ for C++
CC = gcc
VPATH = src: includes

# compiler flags:
#  -g    adds debugging information to the executable file
#  -Wall turns on most, but not all, compiler warnings
CFLAGS  = -g
CFLAGS1 = -Wall
CFLAGS2 = -pedantic 

#different options for different objects example
hello.o: CFLAGS += $(CFLAGS1)
functions.o: CFLAGS += $(CFLAGS2)

#find sources here

INCLUDES = -I includes

#this could be automated if needed
SRCS = hello.c functions.c

#Create object list
OBJS = $(SRCS:.c=.o)

#The main executable
MAIN=hello

.PHONY: clean

all: $(MAIN)
	@echo ------------------------------------------------------------------------------
	@echo "Makefile ALL part done. That's all folks!"
	@echo ------------------------------------------------------------------------------

#just debugging during makefile development
debug: 
	@echo ------------------------------------------------------------------------------
	@echo SOURCES:$(SRCS)
	@echo OBJECTS:$(OBJS)
	@echo INCLUDES:$(INCLUDES)
	@echo ------------------------------------------------------------------------------
	

$(MAIN): $(OBJS)
	@echo ------------------------------------------------------------------------------
	@echo Creating executable:$(MAIN)
	$(CC) $(CFLAGS) $(INCLUDES) -o $(MAIN) $(OBJS) 
	@echo ------------------------------------------------------------------------------

#these objects to be compiled like this. That is our listed source files
$(OBJS): %.o: %.c
	$(CC) $(CFLAGS) $(INCLUDES) -c $<  -o $@


#Normal clean
clean:
	$(RM) *.o *~ $(MAIN)

#Clean generated dependecy files	
cleandeps: 
	$(RM) *.d
	

#Dependecy generation, it just works. See following refrences	
#http://make.mad-scientist.net/papers/advanced-auto-dependency-generation/#tldr
#https://www.gnu.org/software/make/manual/html_node/Automatic-Prerequisites.html
	
%.d: %.c
	@set -e; rm -f $@; \
	$(CC) -M $(CPPFLAGS) $(INCLUDES) $< > $@.$$$$; \
	sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' < $@.$$$$ > $@; \
	rm -f $@.$$$$
         
include $(SRCS:.c=.d)
